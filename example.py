from functools import reduce

def factorial(n: int) -> int:
    """
        Pretty common mathematical operation because I'm uncreative.
        Verified against: https://www.calculatorsoup.com/calculators/discretemathematics/factorials.php
        Examples:

        >>> factorial(0)
        1
        >>> factorial(1)
        1
        >>> factorial(2)
        2
        >>> factorial(12)
        479001600
        >>> factorial(-1)
        Traceback (most recent call last):
            ...
        ValueError: Cannot create factorial of negative integer: -1
        >>> factorial("sheep")
        Traceback (most recent call last):
            ...
        TypeError: Cannot create factorial of non-integer: sheep
    """
    if type(n) != int:
        raise TypeError(f"Cannot create factorial of non-integer: {n}")
    if n < 0:
        raise ValueError(f"Cannot create factorial of negative integer: {n}")
    result = 1
    if n > 1:
        result = reduce(lambda x,y: x*y, range(1, n+1))
    return result


if __name__ == "__main__":
    for n in range(0,50):
        print(f"{n}:", factorial(n))